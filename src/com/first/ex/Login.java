package com.first.ex;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Login extends HttpServlet {
 protected void doPost(HttpServletRequest request, HttpServletResponse response)
 throws ServletException, IOException {
     response.setContentType("text/html;charset=UTF-8");
     PrintWriter out = response.getWriter();	
 String email= request.getParameter("email");
 String pass= request.getParameter("pass");
  if (Validate.checkUser (email , pass))
  {
	  RequestDispatcher rs = request.getRequestDispatcher("Welcome.java");
	  rs.forward(request, response);
  }
  else
  {
	  out.println("Bad USERNAME or PASSOWRD");
	  RequestDispatcher rs = request.getRequestDispatcher("Welcome.html");
	  rs.forward(request, response);
  }
}
}
